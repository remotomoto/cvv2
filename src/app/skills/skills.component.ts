import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Skill} from "./skill";

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements OnInit {

  private _skills: Skill[] = [];

  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {
    this.http.get('assets/data/skills.json')
      .subscribe((skills: Skill[]) => {
        this._skills = skills;
      });
  }


  getSkills(): Skill[] {
    return this._skills;
  }
}
