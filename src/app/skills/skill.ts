export interface Skill {
  category: string;
  text: string;
}
