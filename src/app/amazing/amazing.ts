export interface Amazing {
  title: string;
  text: string;
  name: string;
  url: string;
}
