import {Component, OnInit} from '@angular/core';
import {Amazing} from "./amazing";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-amazing',
  templateUrl: './amazing.component.html',
  styleUrls: ['./amazing.component.scss']
})
export class AmazingComponent implements OnInit {

  private _amazing: Amazing;

  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {
    this.http.get('assets/data/amazing.json')
      .subscribe((amazing: Amazing) => {
        this._amazing = amazing;
      });
  }


  getAmazingTitle(): string {
    return this._amazing ? this._amazing.title : '';
  }

  getAmazingText(): string {
    return this._amazing ? this._amazing.text : '';
  }

  getAmazingName(): string {
    return this._amazing ? this._amazing.name : '';
  }

  getAmazingUrl(): string {
    return this._amazing ? this._amazing.url : '';
  }
}
