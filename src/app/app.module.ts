import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {NavBarComponent} from './nav-bar/nav-bar.component';
import {SkillsComponent} from './skills/skills.component';
import {EmploymentComponent} from './employment/employment.component';
import {SummaryComponent} from './summary/summary.component';
import {EducationComponent} from './education/education.component';

import {HttpClientModule} from '@angular/common/http';
import {FooterComponent} from './footer/footer.component';
import { ExperienceComponent } from './experience/experience.component';
import { AmazingComponent } from './amazing/amazing.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    SkillsComponent,
    EmploymentComponent,
    SummaryComponent,
    EducationComponent,
    FooterComponent,
    ExperienceComponent,
    AmazingComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
