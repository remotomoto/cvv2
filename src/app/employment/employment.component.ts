import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Employment, Project} from "./employment";

@Component({
  selector: 'app-employment',
  templateUrl: './employment.component.html',
  styleUrls: ['./employment.component.scss']
})
export class EmploymentComponent implements OnInit {

  private _employments: Employment[] = [];

  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {
    this.http.get('assets/data/employment.json')
      .subscribe((employments: Employment[]) => {
        this._employments = employments;
      });
  }

  hasUrl(project: Project): boolean {
    return project.url !== ""
  }

  hasNoUrl(project: Project): boolean {
    return project.url === ""
  }

  getEmployments(): Employment[] {
    return this._employments;
  }
}
