export interface Company {
  name: string;
  url: string;
  image: string;
}

export interface Project {
  name: string;
  url: string;
  description: string;
}

export interface Employment {
  years: string;
  position: string;
  company: Company;
  projects: Project[];
  achievements: string[];
  keyAchievement: string;
}
