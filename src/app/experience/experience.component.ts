import {Component, OnInit} from '@angular/core';
import {Experience} from "./experience";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.scss']
})
export class ExperienceComponent implements OnInit {

  private _experiences: Experience[] = [];

  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {
    this.http.get('assets/data/experience.json')
      .subscribe((experiences: Experience[]) => {
        this._experiences = experiences;
      });
  }

  getExperiences(): Experience[] {
    return this._experiences;
  }

}
