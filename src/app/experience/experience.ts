export interface Experience {
  category: string;
  years: number
}
