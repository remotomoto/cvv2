import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Summary} from "./summary";

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {

  private _summary: Summary;

  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {
    this.http.get('assets/data/summary.json')
      .subscribe((summary: Summary) => {
        this._summary = summary;
      });
  }

  getName(): String {
    return this._summary ? this._summary.name : null;
  }

  getObjective(): String {
    return this._summary ? this._summary.objective : null;
  }

  getSummary(): String[] {
    return this._summary ? this._summary.summary : null;
  }

  getImage(): String {
    return this._summary ? this._summary.image : null;
  }

}
