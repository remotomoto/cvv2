export interface Summary {
  image: string;
  name: string;
  objective: string;
  summary: string[];
}
